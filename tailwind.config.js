// tailwind.config.js
module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
    './src/**/*.ts',
    './src/**/*.tsx'
  ],
  theme: {},
  variants: {
     extend: {
      backgroundColor: ['active'],
      opacity: ['active']
    }
  },
  plugins: [
    require('flowbite/plugin')
  ],

  content: [
    "./node_modules/flowbite/**/*.js"
  ]

}